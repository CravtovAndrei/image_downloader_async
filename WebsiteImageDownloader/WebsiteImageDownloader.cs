﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using HtmlAgilityPack;

public class WebsiteImageDownloader
{
    static string _mainSaveFolderPath = "C:\\DownloadedFiles\\";
    static string _websiteToDownloadPageURL = "";//here put your website
    static NetworkCredentials _myCredentials = new NetworkCredentials{UserName = "", Password = ""};
    
    static HTMLAttributesToReplace _htmlAttributesToReplace = new HTMLAttributesToReplace
        {Original = @"/thumb/", NewOne = "/origin/"};
    
    static string _htmlItemAttribute = "src";//
    static string _htmlINodeAttribute = "href";//
    static string _htmlNodeToSearch = "//img";
    static string _htmlCSSClassNodeToUse = "//div[@class='items justified']";
    static string _htmlHeaderToInsert = "User-Agent: Other";
    
    static List<string> _allDownloadableURLs = new List<string>();


    public static async Task Main(string[] args)
    {
        GenerateAllPages();

        foreach (var url in _allDownloadableURLs)
        {
            var mp = new HtmlWeb();
            var mainPage = mp.Load(url);
            await ParseEachPage(mainPage);
        }
    }

    static async Task ParseEachPage(HtmlDocument mainPage)
    {
        var listOfLinksByDivClassFromMainPage =
            mainPage.DocumentNode.SelectSingleNode(_htmlCSSClassNodeToUse); 
        
        var linksFromMainpage = new List<string>();

        if (listOfLinksByDivClassFromMainPage != null)
        {
            linksFromMainpage = listOfLinksByDivClassFromMainPage.Descendants("a").Select(a => a.Attributes[_htmlINodeAttribute].Value).Distinct()
                .ToList();
        }

        var sortedMainPageList = new List<string>();
        
        foreach (var VARIABLE in linksFromMainpage)
        {
            if (!VARIABLE.Contains("page"))
            {
                sortedMainPageList.Add(VARIABLE);
            }
        }

        foreach (var link in sortedMainPageList)
        {
            var h = new HtmlWeb();
            var d = h.Load(link);
            var locUriBuild = new UriBuilder(link).Uri;
            var locTitle = "";

            try
            {
                locTitle = locUriBuild.Segments[2];
            }
            catch (Exception e)
            {
                locTitle = "other";
                Console.WriteLine(e);
            }

            var newTempPath = _mainSaveFolderPath + locTitle;
            

            await ImagedDownloader(d, newTempPath, locTitle);
        }
    }

    static void CreateDirectory(string newTempPath)
    {
        if (!Directory.Exists(newTempPath))
        {
            Directory.CreateDirectory(newTempPath);
        }
    }

    public static void GenerateAllPages()
    {
        _allDownloadableURLs.Add(_websiteToDownloadPageURL);
        for (int i = 2; i < 2610; i++)
        {
            var nextURL = _websiteToDownloadPageURL + $"page{i}";
            _allDownloadableURLs.Add(nextURL);
        }

    }

    static async Task ImagedDownloader(HtmlDocument doc, string path, string title)
    {
        var listOfLinks = doc.DocumentNode.SelectNodes(_htmlNodeToSearch).ToList()
            .Where(item => item.Attributes[_htmlItemAttribute].Value.Contains("https"))
            .Where(nextItem => nextItem.Attributes[_htmlItemAttribute].Value.Contains("jpg")).ToList();

        foreach (var replace in listOfLinks)
        {
            replace.Attributes[_htmlItemAttribute].Value = Regex.Replace(replace.Attributes[_htmlItemAttribute].Value, 
                _htmlAttributesToReplace.Original , _htmlAttributesToReplace.NewOne );
        }

        int i = 0;
        var tasks = new List<Task>();

        foreach (var item in listOfLinks)
        {
            HtmlAttribute att = item.Attributes[_htmlItemAttribute];
            Console.WriteLine(att.Value);
            
            tasks.Add(DownloadImage(att.Value, path,title + i));
            i++;
        }

        try
        {
            
            await Task.Factory.ContinueWhenAll(tasks.ToArray(), completedTasks => { Console.WriteLine("Tasks Completed"); });
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }

    static async Task DownloadImage(string url, string path, string name)
    {
        try
        {
            CreateDirectory(path);
            using (var clientImage = new WebClient())
            {
                clientImage.Credentials = new NetworkCredential(_myCredentials.UserName, _myCredentials.Password);
                clientImage.Headers.Add(_htmlHeaderToInsert); // depends of the website
                var randomNumber = new Random().Next();

                
                await clientImage.DownloadFileTaskAsync(new Uri(url), $"{path}{Remove_Special_Characters( name + randomNumber)}.jpg");
                clientImage.DownloadFileCompleted += Completed;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
    
    public static string Remove_Special_Characters(string str)
    {
        var sb = new StringBuilder();
        foreach (var c in str)
        {
            if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || c == '_' || c == '-')
            {
                sb.Append(c);
            }
        }
        return sb.ToString();
    }
 
    public static void Completed(object o, AsyncCompletedEventArgs args)
    {
        Console.WriteLine("Completed");
    }

    struct NetworkCredentials
    {
        public string UserName;
        public string Password;
    }

    struct HTMLAttributesToReplace
    {
        public string Original;
        public string NewOne;
    }
}


